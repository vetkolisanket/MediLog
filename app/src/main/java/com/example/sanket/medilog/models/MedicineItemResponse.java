package com.example.sanket.medilog.models;

import com.google.gson.annotations.SerializedName;

/**
 * Created by sanket on 5/8/2016.
 */
public class MedicineItemResponse {

    public int id;

    public int uip;

    public int hkpDrugCode;

    public String slug;

    public int su;

    public String meta;

    public String imgUrl;

    @SerializedName("drug_form")
    public String drugForm;

    public boolean available;

    public float oPrice;

    public String label;

    public String type;

    public int mfId;

    public String packForm;

    public String productsForBrand;

    public int discountPerc;

    public boolean prescriptionRequired;

    public String pForm;

    public String manufacturer;

    public String name;

    public String form;

    public float mrp;

    public float uPrice;

    public String packSizeLabel;

    public String packSize;

    public String generics;

    public String mappedPForm;
}
