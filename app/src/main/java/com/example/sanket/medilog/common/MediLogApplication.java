package com.example.sanket.medilog.common;

import android.app.Application;
import android.content.Context;
import android.content.SharedPreferences;

/**
 * Created by sanket on 5/8/2016.
 */
public class MediLogApplication extends Application {

    public static Context APP_CONTEXT;
    public static SharedPreferences SHARED_PREF;
    public static final String IS_DATA_STORED = "isDataStored";

    @Override
    public void onCreate() {
        super.onCreate();

        APP_CONTEXT = this;
        SHARED_PREF = getSharedPreferences("MEDILOG", MODE_PRIVATE);
    }

}
