package com.example.sanket.medilog.database;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.database.sqlite.SQLiteStatement;
import android.text.TextUtils;

import com.example.sanket.medilog.common.MediLogApplication;
import com.example.sanket.medilog.models.MedicineItemResponse;

import java.util.List;

/**
 * Created by sanket on 5/8/2016.
 */
public class DatabaseManager extends SQLiteOpenHelper {

    private static DatabaseManager mDatabaseManager;

    public static DatabaseManager getInstance() {
        if (mDatabaseManager == null) {
            mDatabaseManager = new DatabaseManager(MediLogApplication.APP_CONTEXT, MediLogContract.DB_NAME,
                    null, MediLogContract.DB_VERSION);
        }
        return mDatabaseManager;
    }


    private DatabaseManager(Context context, String name, SQLiteDatabase.CursorFactory factory, int version) {
        super(context, name, factory, version);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        String CREATE_TABLE_MEDILOG = "CREATE TABLE " + MediLogContract.MediLog.TABLE_NAME + " ( " +
                MediLogContract.MediLog.COLUMN_ID + " INTEGER PRIMARY KEY, " +
                MediLogContract.MediLog.COLUMN_UIP + " INTEGER, " +
                MediLogContract.MediLog.COLUMN_HKP_DRUG_CODE + " INTEGER, " +
                MediLogContract.MediLog.COLUMN_SLUG + " TEXT, " +
                MediLogContract.MediLog.COLUMN_SU + " INTEGER, " +
                MediLogContract.MediLog.COLUMN_META + " TEXT, " +
                MediLogContract.MediLog.COLUMN_IMG_URL + " TEXT, " +
                MediLogContract.MediLog.COLUMN_DRUG_FORM + " TEXT, " +
                MediLogContract.MediLog.COLUMN_AVAILABLE + " INTEGER, " +
                MediLogContract.MediLog.COLUMN_O_PRICE + " REAL, " +
                MediLogContract.MediLog.COLUMN_LABEL + " TEXT, " +
                MediLogContract.MediLog.COLUMN_TYPE + " TEXT, " +
                MediLogContract.MediLog.COLUMN_MF_ID + " INTEGER, " +
                MediLogContract.MediLog.COLUMN_PACK_FORM + " TEXT, " +
                MediLogContract.MediLog.COLUMN_PRODUCTS_FOR_BRAND + " TEXT, " +
                MediLogContract.MediLog.COLUMN_DISCOUNT_PERC + " INTEGER, " +
                MediLogContract.MediLog.COLUMN_PRESCRIPTION_REQUIRED + " INTEGER, " +
                MediLogContract.MediLog.COLUMN_P_FORM + " TEXT, " +
                MediLogContract.MediLog.COLUMN_MANUFACTURER + " TEXT, " +
                MediLogContract.MediLog.COLUMN_NAME + " TEXT, " +
                MediLogContract.MediLog.COLUMN_FORM + " TEXT, " +
                MediLogContract.MediLog.COLUMN_MRP + " REAL, " +
                MediLogContract.MediLog.COLUMN_U_PRICE + " REAL, " +
                MediLogContract.MediLog.COLUMN_PACK_SIZE_LABEL + " TEXT, " +
                MediLogContract.MediLog.COLUMN_PACK_SIZE + " TEXT, " +
                MediLogContract.MediLog.COLUMN_GENERICS + " TEXT, " +
                MediLogContract.MediLog.COLUMN_MAPPED_P_FORM + " TEXT )";

        db.execSQL(CREATE_TABLE_MEDILOG);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        //in case of db upgrade
    }

    public boolean isMediLogPresentInDB(int id) {
        String Query = "SELECT * FROM " +
                MediLogContract.MediLog.TABLE_NAME +
                " WHERE " + MediLogContract.MediLog.COLUMN_ID + " = " + id;
        Cursor cursor = getReadableDatabase().rawQuery(Query, null);
        if (cursor.getCount() <= 0) {
            cursor.close();
            return false;
        }
        cursor.close();
        return true;
    }

    public boolean insertMediLogs(List<MedicineItemResponse> medicineItemResponseList) {
        String INSERT_INTO_MEDILOG = "INSERT INTO " + MediLogContract.MediLog.TABLE_NAME + " ( " +
                MediLogContract.MediLog.COLUMN_ID + "," +
                MediLogContract.MediLog.COLUMN_UIP + "," +
                MediLogContract.MediLog.COLUMN_HKP_DRUG_CODE + "," +
                MediLogContract.MediLog.COLUMN_SLUG + "," +
                MediLogContract.MediLog.COLUMN_SU + "," +
                MediLogContract.MediLog.COLUMN_META + "," +
                MediLogContract.MediLog.COLUMN_IMG_URL + "," +
                MediLogContract.MediLog.COLUMN_DRUG_FORM + "," +
                MediLogContract.MediLog.COLUMN_AVAILABLE + "," +
                MediLogContract.MediLog.COLUMN_O_PRICE + "," +
                MediLogContract.MediLog.COLUMN_LABEL + "," +
                MediLogContract.MediLog.COLUMN_TYPE + "," +
                MediLogContract.MediLog.COLUMN_MF_ID + "," +
                MediLogContract.MediLog.COLUMN_PACK_FORM + "," +
                MediLogContract.MediLog.COLUMN_PRODUCTS_FOR_BRAND + "," +
                MediLogContract.MediLog.COLUMN_DISCOUNT_PERC + "," +
                MediLogContract.MediLog.COLUMN_PRESCRIPTION_REQUIRED + "," +
                MediLogContract.MediLog.COLUMN_P_FORM + "," +
                MediLogContract.MediLog.COLUMN_MANUFACTURER + "," +
                MediLogContract.MediLog.COLUMN_NAME + "," +
                MediLogContract.MediLog.COLUMN_FORM + "," +
                MediLogContract.MediLog.COLUMN_MRP + "," +
                MediLogContract.MediLog.COLUMN_U_PRICE + "," +
                MediLogContract.MediLog.COLUMN_PACK_SIZE_LABEL + "," +
                MediLogContract.MediLog.COLUMN_PACK_SIZE + "," +
                MediLogContract.MediLog.COLUMN_GENERICS + "," +
                MediLogContract.MediLog.COLUMN_MAPPED_P_FORM + " ) " +
                "VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?) ";

        SQLiteDatabase database = getWritableDatabase();
        SQLiteStatement statement = database.compileStatement(INSERT_INTO_MEDILOG);
        try {
            database.beginTransaction();
            for (int i = 0, size = medicineItemResponseList.size(); i < size; i++) {
                MedicineItemResponse medicineItem = medicineItemResponseList.get(i);
                if (!isMediLogPresentInDB(medicineItem.id)) {
                    statement.clearBindings();
                    statement.bindLong(1, medicineItem.id);
                    statement.bindLong(2, medicineItem.uip);
                    statement.bindLong(3, medicineItem.hkpDrugCode);
                    statement.bindString(4, TextUtils.isEmpty(medicineItem.slug) ? "" : medicineItem.slug);
                    statement.bindLong(5, medicineItem.su);
                    statement.bindString(6, TextUtils.isEmpty(medicineItem.meta) ? "" : medicineItem.meta);
                    statement.bindString(7, TextUtils.isEmpty(medicineItem.imgUrl) ? "" : medicineItem.imgUrl);
                    statement.bindString(8, TextUtils.isEmpty(medicineItem.drugForm) ? "" : medicineItem.drugForm);
                    statement.bindLong(9, medicineItem.available ? 1 : 0);
                    statement.bindDouble(10, medicineItem.oPrice);
                    statement.bindString(11, TextUtils.isEmpty(medicineItem.label) ? "" : medicineItem.label);
                    statement.bindString(12, TextUtils.isEmpty(medicineItem.type) ? "" : medicineItem.type);
                    statement.bindLong(13, medicineItem.mfId);
                    statement.bindString(14, TextUtils.isEmpty(medicineItem.packForm) ? "" : medicineItem.packForm);
                    statement.bindString(15, TextUtils.isEmpty(medicineItem.productsForBrand) ? "" : medicineItem.productsForBrand);
                    statement.bindLong(16, medicineItem.discountPerc);
                    statement.bindLong(17, medicineItem.prescriptionRequired ? 1 : 0);
                    statement.bindString(18, TextUtils.isEmpty(medicineItem.pForm) ? "" : medicineItem.pForm);
                    statement.bindString(19, TextUtils.isEmpty(medicineItem.manufacturer) ? "" : medicineItem.manufacturer);
                    statement.bindString(20, TextUtils.isEmpty(medicineItem.name) ? "" : medicineItem.name);
                    statement.bindString(21, TextUtils.isEmpty(medicineItem.form) ? "" : medicineItem.form);
                    statement.bindDouble(22, medicineItem.mrp);
                    statement.bindDouble(23, medicineItem.uPrice);
                    statement.bindString(24, TextUtils.isEmpty(medicineItem.packSizeLabel) ? "" : medicineItem.packSizeLabel);
                    statement.bindString(25, TextUtils.isEmpty(medicineItem.packSize) ? "" : medicineItem.packSize);
                    statement.bindString(26, TextUtils.isEmpty(medicineItem.generics) ? "" : medicineItem.generics);
                    statement.bindString(27, TextUtils.isEmpty(medicineItem.mappedPForm) ? "" : medicineItem.mappedPForm);
                    statement.executeInsert();
                }
            }
            database.setTransactionSuccessful();
            return true;
        } catch (Exception e) {
            return false;
        } finally {
            database.endTransaction();
        }

    }

    public Cursor getMediLogs() {
        String SELECT_MEDILOGS = "SELECT * FROM " + MediLogContract.MediLog.TABLE_NAME;
        return getReadableDatabase().rawQuery(SELECT_MEDILOGS, null);
    }
}
