package com.example.sanket.medilog.asynctasks;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;

import com.example.sanket.medilog.common.MediLogApplication;
import com.example.sanket.medilog.database.DatabaseManager;
import com.example.sanket.medilog.models.MedicineItemResponse;

import java.lang.ref.WeakReference;
import java.util.List;

/**
 * Created by sanket on 5/8/2016.
 */
public class InsertMediLogsTask extends AsyncTask<Void, Void, Boolean> {

    private WeakReference<Context> mContextWeakReference;
    private WeakReference<List<MedicineItemResponse>> mListWeakReference;
    private WeakReference<ProgressDialog> mProgressDialogWeakReference;

    public InsertMediLogsTask(Context context, List<MedicineItemResponse> medicineItemResponseList, ProgressDialog progressDialog) {
        mContextWeakReference = new WeakReference<>(context);
        mListWeakReference = new WeakReference<>(medicineItemResponseList);
        mProgressDialogWeakReference = new WeakReference<>(progressDialog);
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        Context context = mContextWeakReference.get();
        if (context != null) {
            ProgressDialog progressDialog = mProgressDialogWeakReference.get();
            if (progressDialog != null) {
                progressDialog.show();
            }
        }
    }

    @Override
    protected Boolean doInBackground(Void... params) {
        Context context = mContextWeakReference.get();
        if (context != null) {
            List<MedicineItemResponse> medicineItemResponseList = mListWeakReference.get();
            if (medicineItemResponseList != null) {
                DatabaseManager databaseManager = DatabaseManager.getInstance();
                return databaseManager.insertMediLogs(medicineItemResponseList);
            }
        }
        return false;
    }

    @Override
    protected void onPostExecute(Boolean result) {
        super.onPostExecute(result);
        Context context = mContextWeakReference.get();
        if (context != null) {
            ProgressDialog progressDialog = mProgressDialogWeakReference.get();
            if (result != null && result) {
                MediLogApplication.SHARED_PREF.edit().putBoolean(MediLogApplication.IS_DATA_STORED, true).apply();
                new FetchMediLogsTask(context, progressDialog).execute();
            }
            if (progressDialog != null) {
                progressDialog.dismiss();
            }
        }
    }
}
