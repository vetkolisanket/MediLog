package com.example.sanket.medilog.adapters;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import com.example.sanket.medilog.R;
import com.example.sanket.medilog.fragments.MedicineInfoFragment;
import com.example.sanket.medilog.models.MedicineItemResponse;

import java.util.List;

/**
 * Created by sanket on 5/9/2016.
 */
public class MediLogPagerAdapter extends FragmentStatePagerAdapter {

    List<MedicineItemResponse> mMedicineItemResponseList;

    public MediLogPagerAdapter(FragmentManager fm, List<MedicineItemResponse> medicineItemResponseList) {
        super(fm);
        mMedicineItemResponseList = medicineItemResponseList;
    }

    @Override
    public Fragment getItem(int position) {
        return MedicineInfoFragment.newInstance(R.mipmap.medicine);
    }

    @Override
    public int getCount() {
        return mMedicineItemResponseList.size();
    }
}
