package com.example.sanket.medilog.asynctasks;

import android.app.ProgressDialog;
import android.content.Context;
import android.database.Cursor;
import android.os.AsyncTask;

import com.example.sanket.medilog.activities.MedicineViewActivity;
import com.example.sanket.medilog.database.DatabaseManager;
import com.example.sanket.medilog.database.MediLogContract;
import com.example.sanket.medilog.models.MedicineItemResponse;

import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by sanket on 5/8/2016.
 */
public class FetchMediLogsTask extends AsyncTask<Void, Void, List<MedicineItemResponse>> {

    private WeakReference<Context> mContextWeakReference;
    private WeakReference<ProgressDialog> mProgressDialogWeakReference;

    public FetchMediLogsTask(Context context, ProgressDialog progressDialog) {
        mContextWeakReference = new WeakReference<>(context);
        mProgressDialogWeakReference = new WeakReference<>(progressDialog);
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        Context context = mContextWeakReference.get();
        if (context != null) {
            ProgressDialog progressDialog = mProgressDialogWeakReference.get();
            if (progressDialog != null) {
                progressDialog.setMessage("Fetching data from DB");
                progressDialog.show();
            }
        }
    }

    @Override
    protected List<MedicineItemResponse> doInBackground(Void... params) {
        Context context = mContextWeakReference.get();
        if (context != null) {
            DatabaseManager databaseManager = DatabaseManager.getInstance();
            Cursor cursor = databaseManager.getMediLogs();
            if (cursor != null) {
                List<MedicineItemResponse> medicineItemResponseList = new ArrayList<>();
                int INDEX_COLUMN_ID = cursor.getColumnIndex(MediLogContract.MediLog.COLUMN_ID);
                int INDEX_COLUMN_UIP = cursor.getColumnIndex(MediLogContract.MediLog.COLUMN_UIP);
                int INDEX_COLUMN_HKP_DRUG_CODE = cursor.getColumnIndex(MediLogContract.MediLog.COLUMN_HKP_DRUG_CODE);
                int INDEX_COLUMN_SLUG = cursor.getColumnIndex(MediLogContract.MediLog.COLUMN_SLUG);
                int INDEX_COLUMN_SU = cursor.getColumnIndex(MediLogContract.MediLog.COLUMN_SU);
                int INDEX_COLUMN_META = cursor.getColumnIndex(MediLogContract.MediLog.COLUMN_META);
                int INDEX_COLUMN_IMG_URL = cursor.getColumnIndex(MediLogContract.MediLog.COLUMN_IMG_URL);
                int INDEX_COLUMN_DRUG_FORM = cursor.getColumnIndex(MediLogContract.MediLog.COLUMN_DRUG_FORM);
                int INDEX_COLUMN_AVAILABLE = cursor.getColumnIndex(MediLogContract.MediLog.COLUMN_AVAILABLE);
                int INDEX_COLUMN_O_PRICE = cursor.getColumnIndex(MediLogContract.MediLog.COLUMN_O_PRICE);
                int INDEX_COLUMN_LABEL = cursor.getColumnIndex(MediLogContract.MediLog.COLUMN_LABEL);
                int INDEX_COLUMN_TYPE = cursor.getColumnIndex(MediLogContract.MediLog.COLUMN_TYPE);
                int INDEX_COLUMN_MF_ID = cursor.getColumnIndex(MediLogContract.MediLog.COLUMN_MF_ID);
                int INDEX_COLUMN_PACK_FORM = cursor.getColumnIndex(MediLogContract.MediLog.COLUMN_PACK_FORM);
                int INDEX_COLUMN_PRODUCTS_FOR_BRAND = cursor.getColumnIndex(MediLogContract.MediLog.COLUMN_PRODUCTS_FOR_BRAND);
                int INDEX_COLUMN_DISCOUNT_PERC = cursor.getColumnIndex(MediLogContract.MediLog.COLUMN_DISCOUNT_PERC);
                int INDEX_COLUMN_PRESCRIPTION_REQUIRED = cursor.getColumnIndex(MediLogContract.MediLog.COLUMN_PRESCRIPTION_REQUIRED);
                int INDEX_COLUMN_P_FORM = cursor.getColumnIndex(MediLogContract.MediLog.COLUMN_P_FORM);
                int INDEX_COLUMN_MANUFACTURER = cursor.getColumnIndex(MediLogContract.MediLog.COLUMN_MANUFACTURER);
                int INDEX_COLUMN_NAME = cursor.getColumnIndex(MediLogContract.MediLog.COLUMN_NAME);
                int INDEX_COLUMN_FORM = cursor.getColumnIndex(MediLogContract.MediLog.COLUMN_FORM);
                int INDEX_COLUMN_MRP = cursor.getColumnIndex(MediLogContract.MediLog.COLUMN_MRP);
                int INDEX_COLUMN_U_PRICE = cursor.getColumnIndex(MediLogContract.MediLog.COLUMN_U_PRICE);
                int INDEX_COLUMN_PACK_SIZE_LABEL = cursor.getColumnIndex(MediLogContract.MediLog.COLUMN_PACK_SIZE_LABEL);
                int INDEX_COLUMN_PACK_SIZE = cursor.getColumnIndex(MediLogContract.MediLog.COLUMN_PACK_SIZE);
                int INDEX_COLUMN_GENERICS = cursor.getColumnIndex(MediLogContract.MediLog.COLUMN_GENERICS);
                int INDEX_COLUMN_MAPPED_P_FORM = cursor.getColumnIndex(MediLogContract.MediLog.COLUMN_MAPPED_P_FORM);
                while (cursor.moveToNext()) {
                    MedicineItemResponse medicineItem = new MedicineItemResponse();
                    medicineItem.id = (int) cursor.getLong(INDEX_COLUMN_ID);
                    medicineItem.uip = (int) cursor.getLong(INDEX_COLUMN_UIP);
                    medicineItem.hkpDrugCode = (int) cursor.getLong(INDEX_COLUMN_HKP_DRUG_CODE);
                    medicineItem.slug = cursor.getString(INDEX_COLUMN_SLUG);
                    medicineItem.su = (int) cursor.getLong(INDEX_COLUMN_SU);
                    medicineItem.meta = cursor.getString(INDEX_COLUMN_META);
                    medicineItem.imgUrl = cursor.getString(INDEX_COLUMN_IMG_URL);
                    medicineItem.drugForm = cursor.getString(INDEX_COLUMN_DRUG_FORM);
                    medicineItem.available = cursor.getLong(INDEX_COLUMN_AVAILABLE) == 1;
                    medicineItem.oPrice = (float) cursor.getDouble(INDEX_COLUMN_O_PRICE);
                    medicineItem.label = cursor.getString(INDEX_COLUMN_LABEL);
                    medicineItem.type = cursor.getString(INDEX_COLUMN_TYPE);
                    medicineItem.mfId = (int) cursor.getLong(INDEX_COLUMN_MF_ID);
                    medicineItem.packForm = cursor.getString(INDEX_COLUMN_PACK_FORM);
                    medicineItem.productsForBrand = cursor.getString(INDEX_COLUMN_PRODUCTS_FOR_BRAND);
                    medicineItem.discountPerc = (int) cursor.getLong(INDEX_COLUMN_DISCOUNT_PERC);
                    medicineItem.prescriptionRequired = cursor.getLong(INDEX_COLUMN_PRESCRIPTION_REQUIRED) == 1;
                    medicineItem.pForm = cursor.getString(INDEX_COLUMN_P_FORM);
                    medicineItem.manufacturer = cursor.getString(INDEX_COLUMN_MANUFACTURER);
                    medicineItem.name = cursor.getString(INDEX_COLUMN_NAME);
                    medicineItem.form = cursor.getString(INDEX_COLUMN_FORM);
                    medicineItem.mrp = (float) cursor.getDouble(INDEX_COLUMN_MRP);
                    medicineItem.uPrice = (float) cursor.getDouble(INDEX_COLUMN_U_PRICE);
                    medicineItem.packSizeLabel = cursor.getString(INDEX_COLUMN_PACK_SIZE_LABEL);
                    medicineItem.packSize = cursor.getString(INDEX_COLUMN_PACK_SIZE);
                    medicineItem.generics = cursor.getString(INDEX_COLUMN_GENERICS);
                    medicineItem.mappedPForm = cursor.getString(INDEX_COLUMN_MAPPED_P_FORM);
                    medicineItemResponseList.add(medicineItem);
                }
                cursor.close();
                return medicineItemResponseList;
            }
        }
        return null;
    }

    @Override
    protected void onPostExecute(List<MedicineItemResponse> medicineItemResponseList) {
        super.onPostExecute(medicineItemResponseList);
        Context context = mContextWeakReference.get();
        if (context != null) {
            ProgressDialog progressDialog = mProgressDialogWeakReference.get();
            if (medicineItemResponseList != null) {
                ((MedicineViewActivity) context).updateList(medicineItemResponseList);
            }
            if (progressDialog != null) {
                progressDialog.dismiss();
            }
        }
    }
}
