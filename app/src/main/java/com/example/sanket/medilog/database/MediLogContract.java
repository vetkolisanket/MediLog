package com.example.sanket.medilog.database;

/**
 * Created by sanket on 5/8/2016.
 */
public class MediLogContract {

    private MediLogContract() {}

    public static final String DB_NAME = "MediLog.db";

    public static final int DB_VERSION = 1;

    public static class MediLog {
        public static final String TABLE_NAME = "mediLog";

        public static final String COLUMN_ID = "id";

        public static final String COLUMN_UIP = "uip";

        public static final String COLUMN_HKP_DRUG_CODE = "hkpDrugCode";

        public static final String COLUMN_SLUG = "slug";

        public static final String COLUMN_SU = "su";

        public static final String COLUMN_META = "meta";

        public static final String COLUMN_IMG_URL = "imgUrl";

        public static final String COLUMN_DRUG_FORM = "drugForm";

        public static final String COLUMN_AVAILABLE = "available";

        public static final String COLUMN_O_PRICE = "oPrice";

        public static final String COLUMN_LABEL = "label";

        public static final String COLUMN_TYPE = "type";

        public static final String COLUMN_MF_ID = "mfId";

        public static final String COLUMN_PACK_FORM = "packForm";

        public static final String COLUMN_PRODUCTS_FOR_BRAND = "productsForBrand";

        public static final String COLUMN_DISCOUNT_PERC = "discountPerc";

        public static final String COLUMN_PRESCRIPTION_REQUIRED = "prescriptionRequired";

        public static final String COLUMN_P_FORM = "pForm";

        public static final String COLUMN_MANUFACTURER = "manufacturer";

        public static final String COLUMN_NAME = "name";

        public static final String COLUMN_FORM = "form";

        public static final String COLUMN_MRP = "mrp";

        public static final String COLUMN_U_PRICE = "uPrice";

        public static final String COLUMN_PACK_SIZE_LABEL = "packSizeLabel";

        public static final String COLUMN_PACK_SIZE = "packSize";

        public static final String COLUMN_GENERICS = "generics";

        public static final String COLUMN_MAPPED_P_FORM = "mappedPForm";
    }

}
