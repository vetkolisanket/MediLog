package com.example.sanket.medilog.activities;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.example.sanket.medilog.R;
import com.example.sanket.medilog.adapters.MediLogPagerAdapter;
import com.example.sanket.medilog.asynctasks.FetchMediLogsTask;
import com.example.sanket.medilog.asynctasks.InsertMediLogsTask;
import com.example.sanket.medilog.common.MediLogApplication;
import com.example.sanket.medilog.models.MedicineItemResponse;
import com.example.sanket.medilog.models.ResponseItems;
import com.example.sanket.medilog.retrofit.APIClient;
import com.squareup.okhttp.OkHttpClient;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

import retrofit.Callback;
import retrofit.RestAdapter;
import retrofit.RetrofitError;
import retrofit.client.OkClient;
import retrofit.client.Response;

public class MedicineViewActivity extends AppCompatActivity {

    private List<MedicineItemResponse> mMedicineItemResponseList;
    private MediLogPagerAdapter mMediLogPagerAdapter;
    private TextView mTVPageCount;
    private TextView mTVManufacturerName;
    private TextView mTVAvailability;
    private TextView mTVMRP;
    //private TextSwitcher mTSPageCount;
    private ViewPager mVPMediLog;
    private ProgressDialog mPDMedicineView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_medicine_view);

        mVPMediLog = (ViewPager) findViewById(R.id.vp_medilog);
        mTVPageCount = (TextView) findViewById(R.id.tv_page_count);
        //mTSPageCount = (TextSwitcher) findViewById(R.id.ts_page_count);
        mTVManufacturerName = (TextView) findViewById(R.id.tv_manufacturer_name);
        mTVAvailability = (TextView) findViewById(R.id.tv_availability);
        mTVMRP = (TextView) findViewById(R.id.tv_mrp);
        mPDMedicineView = new ProgressDialog(this);
        mPDMedicineView.setMessage("Fetching data from server");
        mPDMedicineView.setCancelable(false);

        // Set the ViewFactory of the TextSwitcher that will create TextView object when asked
        /*mTSPageCount.setFactory(new ViewSwitcher.ViewFactory() {

            public View makeView() {
                // create new textView and set the properties like clolr, size etc
                TextView myText = new TextView(MedicineViewActivity.this);
                myText.setGravity(Gravity.TOP | Gravity.CENTER_HORIZONTAL);
                myText.setTextSize(36);
                myText.setTextColor(Color.BLUE);
                return myText;
            }
        });*/

        // Declare the in and out animations and initialize them
        /*Animation in = AnimationUtils.loadAnimation(this,android.R.anim.slide_in_left);
        Animation out = AnimationUtils.loadAnimation(this,android.R.anim.slide_out_right);*/

        /*// set the animation type of textSwitcher
        mTSPageCount.setInAnimation(in);
        mTSPageCount.setOutAnimation(out);*/

        mMedicineItemResponseList = new ArrayList<>();
        mMediLogPagerAdapter = new MediLogPagerAdapter(getSupportFragmentManager(), mMedicineItemResponseList);
        if (mVPMediLog != null) {
            mVPMediLog.setAdapter(mMediLogPagerAdapter);

            mVPMediLog.setPageTransformer(false, new ViewPager.PageTransformer() {
                private static final float MIN_SCALE = 0.75f;

                @Override
                public void transformPage(View page, float position) {
                    if (position >= -1 && position <= 1) {
                        // Fade the page out.
                        page.setAlpha(1 - Math.abs(position));
                        // Scale the page down (between MIN_SCALE and 1)
                        float scaleFactor = MIN_SCALE + (1 - MIN_SCALE) * (1 - Math.abs(position));
                        page.setScaleX(scaleFactor);
                        page.setScaleY(scaleFactor);

                    } else {
                        page.setAlpha(0);
                    }
                }
            });

            mVPMediLog.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {

                @Override
                public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
                }

                @Override
                public void onPageSelected(int position) {
                    updateView();
                }

                @Override
                public void onPageScrollStateChanged(int state) {
                }
            });
        }

        if(!MediLogApplication.SHARED_PREF.getBoolean(MediLogApplication.IS_DATA_STORED, false)) {
            fetchMedicineInfo();
        } else {
            new FetchMediLogsTask(this, mPDMedicineView).execute();
        }
    }

    private void fetchMedicineInfo() {
        final String BASE_URL = "https://www.1mg.com";
        final String name = "b";
        final long pageSize = 1000; //10000000L (using small value for test)
        final long idTime = 1435404923427L;
        mPDMedicineView.show();

        final OkHttpClient okHttpClient = new OkHttpClient();
        okHttpClient.setReadTimeout(60, TimeUnit.SECONDS);
        okHttpClient.setConnectTimeout(60, TimeUnit.SECONDS);

        RestAdapter restAdapter = new RestAdapter.Builder()
                .setEndpoint(BASE_URL)
                .setClient(new OkClient(okHttpClient))
                .build();

        APIClient apiClient = restAdapter.create(APIClient.class);
        Toast.makeText(MedicineViewActivity.this, "This may take some time", Toast.LENGTH_SHORT).show();
        apiClient.fetchMedicinesInfo(name, pageSize, idTime, new Callback<ResponseItems>() {
            @Override
            public void success(ResponseItems responseItems, Response response) {
                //Toast.makeText(MedicineViewActivity.this, "Success", Toast.LENGTH_SHORT).show();
                new InsertMediLogsTask(MedicineViewActivity.this, responseItems.medicineItemResponseList, mPDMedicineView).execute();
            }

            @Override
            public void failure(RetrofitError error) {
                //Toast.makeText(MedicineViewActivity.this, "Failure", Toast.LENGTH_SHORT).show();
                mPDMedicineView.dismiss();
            }
        });
    }

    public void updateList(List<MedicineItemResponse> medicineItemResponseList) {
        mMedicineItemResponseList.addAll(medicineItemResponseList);
        mMediLogPagerAdapter.notifyDataSetChanged();
        updateView();
    }

    public void updateView() {
        int position = mVPMediLog.getCurrentItem();
        String name = mMedicineItemResponseList.get(position).name;
        String manufacturerName = mMedicineItemResponseList.get(position).manufacturer;
        boolean availability = mMedicineItemResponseList.get(position).available;
        float mrp = mMedicineItemResponseList.get(position).mrp;
        mTVPageCount.setText(new StringBuilder().append(position+1).append("/").append(mMedicineItemResponseList.size()));
        //mTSPageCount.setText(new StringBuilder().append(position+1).append("/").append(mMedicineItemResponseList.size()));

        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            if (!TextUtils.isEmpty(name)) {
                actionBar.setSubtitle(name);
            } else {
                actionBar.setSubtitle("");
            }
        }
        if (TextUtils.isEmpty(manufacturerName)) {
            mTVManufacturerName.setText("");
        } else {
            mTVManufacturerName.setText(manufacturerName);
        }

        if (availability) {
            mTVAvailability.setText("Available");
        } else {
            mTVAvailability.setText("Not Available");
        }

        mTVMRP.setText("MRP: "+mrp);
    }
}
