package com.example.sanket.medilog.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.example.sanket.medilog.R;
import com.squareup.picasso.Picasso;

/**
 * Created by sanket on 5/9/2016.
 */
public class MedicineInfoFragment extends Fragment {

    private static final String IMAGE_RESOURCE = "imageResource";

    public static MedicineInfoFragment newInstance(int imageResource) {
        MedicineInfoFragment medicineInfoFragment = new MedicineInfoFragment();
        Bundle bundle = new Bundle();
        bundle.putInt(IMAGE_RESOURCE, imageResource);
        medicineInfoFragment.setArguments(bundle);
        return medicineInfoFragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.adapter_medicine_view, container, false);
        Bundle bundle = getArguments();
        Picasso.with(getActivity())
                .load(bundle.getInt(IMAGE_RESOURCE))
                .into((ImageView) rootView.findViewById(R.id.iv_medicine));

        return rootView;
    }
}
