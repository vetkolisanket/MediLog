package com.example.sanket.medilog.retrofit;

import com.example.sanket.medilog.models.ResponseItems;

import retrofit.Callback;
import retrofit.http.GET;
import retrofit.http.Query;

/**
 * Created by sanket on 5/8/2016.
 */
public interface APIClient {

    @GET("/api/v1/search/autocomplete")
    void fetchMedicinesInfo(@Query("name") String name, @Query("pageSize") long pageSize,
                            @Query("_") long idTime, Callback<ResponseItems> callback);

}
