package com.example.sanket.medilog.models;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by sanket on 5/8/2016.
 */
public class ResponseItems {

    @SerializedName("result")
    public List<MedicineItemResponse> medicineItemResponseList;

}
